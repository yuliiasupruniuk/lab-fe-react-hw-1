import { mockedAuthorsList, mockedCoursesList } from '../constants';
import searchPipe from './search.pipe';
import { v4 as uuidv4 } from 'uuid';
import { formatDate } from './dateGeneratop';

export const ACTION_TYPES = {
	FILTER: 'filter',
	CREATE_COURSE: 'create',
	ADD_AUTHOR: 'add',
};

export const initialState = {
	coursesList: mockedCoursesList,
	authorsList: mockedAuthorsList,
};

let cacheState = {
	coursesList: mockedCoursesList,
	authorsList: mockedAuthorsList,
};

function setCache(state) {
	cacheState = { ...state };
}

export function reducer(state, action) {
	switch (action.type) {
		case ACTION_TYPES.FILTER:
			if (action.src === '') {
				return cacheState;
			}
			return {
				coursesList: searchPipe(action.src, cacheState.coursesList),
				authorsList: cacheState.authorsList,
			};

		case ACTION_TYPES.ADD_AUTHOR:
			let newAuthorList = [...state.authorsList];
			const author = {
				name: action.author,
				id: uuidv4(),
			};
			newAuthorList.push(author);
			const newState = {
				coursesList: state.coursesList,
				authorsList: newAuthorList,
			};
			setCache(newState);
			return newState;

		case ACTION_TYPES.CREATE_COURSE:
			const course = {
				...action.course,
				id: uuidv4(),
				creationDate: formatDate(new Date()),
			};
			const courses = [...state.coursesList];
			courses.push(course);

			const updatedState = {
				coursesList: [...courses],
				authorsList: state.authorsList,
			};
			setCache(updatedState);
			return updatedState;
		default:
			return state;
	}
}
