function dateGenerator(date) {
	return date.replaceAll('/', '.');
}

function formatDate(date) {
	const dd = String(date.getDate());
	const mm = String(date.getMonth() + 1);
	const yyyy = date.getFullYear();

	return dd + '/' + mm + '/' + yyyy;
}

export { dateGenerator, formatDate };
