import Button from '../common/Button/Button';
import Logo from './components/Logo/Logo';
import { username } from '../../constants';

function Header() {
	return (
		<nav className='navbar navbar-light alert-primary text-dark'>
			<a className='navbar-brand' href='/'>
				<Logo />
			</a>

			<div className='d-flex align-items-end'>
				<h6 className='mr-3'>{username}</h6>
				<Button buttonText='Logout' color='warning' />
			</div>
		</nav>
	);
}
export default Header;
