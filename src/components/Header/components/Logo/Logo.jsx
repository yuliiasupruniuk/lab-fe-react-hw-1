import { logoURL } from '../../../../constants';

function Logo() {
	return <img src={logoURL} alt='logo' width='150' height='100' />;
}
export default Logo;
