import { useReducer, useState } from 'react';
import { initialState, reducer } from '../../helpers/reducer';
import Button from '../common/Button/Button';
import CreateCourse from '../CreateCourse/CreateCourse';
import CourseCard from './components/CourseCard/CourseCard';
import SearchBar from './components/SearchBar/SearchBar';

function Courses() {
	const [state, dispatch] = useReducer(reducer, initialState);
	const [create, toggleCreate] = useState(false);

	const findAuthors = (authorsIds) => {
		const authors = state.authorsList.filter(
			(author) => authorsIds.indexOf(author.id) !== -1
		);
		return authors;
	};

	const cousesList = state.coursesList.map((course) => {
		const authors = findAuthors(course.authors);
		return <CourseCard key={course.id} course={course} authors={authors} />;
	});

	const showCreationForm = () => {
		toggleCreate(true);
	};

	const hideCreationForm = () => {
		toggleCreate(false);
	};

	const content = create ? (
		<CreateCourse
			authors={state.authorsList}
			dispatch={dispatch}
			hide={hideCreationForm}
		/>
	) : (
		<div>
			<div className='d-flex justify-content-between my-3'>
				<SearchBar state={state} dispatch={dispatch} />
				<Button buttonText='Add new course' onClick={showCreationForm} />
			</div>
			<ul className='list-group'>{cousesList}</ul>
		</div>
	);

	return content;
}

export default Courses;
