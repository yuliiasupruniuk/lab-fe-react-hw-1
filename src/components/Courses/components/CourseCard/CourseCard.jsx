import Button from '../../../common/Button/Button';
import { dateGenerator } from '../../../../helpers/dateGeneratop';
import durationInHours from '../../../../helpers/pipeDuration';

function CourseCard(props) {
	const authors = props.authors.map((author) => author.name).join(', ');

	const getDuration = () => {
		return durationInHours(props.course.duration);
	};

	const getDate = () => {
		return dateGenerator(props.course.creationDate);
	};

	return (
		<div className='container-fluid border border-primary my-3'>
			<div className='row p-4'>
				<div className='main-info col-9'>
					<h2>{props.course.title}</h2>
					<p>{props.course.description}</p>
				</div>
				<div className='additional-info col-3'>
					<div>
						<p className='text-truncate'>
							<strong>Authors: </strong>
							{authors}
						</p>
						<p>
							<strong>Duration: </strong>
							{getDuration()} hours
						</p>
						<p>
							<strong>Created: </strong>
							{getDate()}
						</p>
					</div>
					<div className='d-flex justify-content-center'>
						<Button buttonText='Show course' />
					</div>
				</div>
			</div>
		</div>
	);
}
export default CourseCard;
