import { useState } from 'react';
import { ACTION_TYPES } from '../../../../helpers/reducer';
import Button from '../../../common/Button/Button';
import Input from '../../../common/Input/Input';

function SearchBar(props) {
	const [src, setSrc] = useState('');

	const dispatch = props.dispatch;

	const searchCourses = (search = src) => {
		dispatch({ type: ACTION_TYPES.FILTER, src: search });
	};

	const setSearch = async (string) => {
		setSrc(string);
		if (string.length === 0 || !string?.trim()) {
			searchCourses('');
		}
	};

	return (
		<div className='row'>
			<Input placeholder='Enter course name or id...' onChange={setSearch} />
			<div className='col-auto'>
				<Button buttonText='Search' onClick={searchCourses} />
			</div>
		</div>
	);
}
export default SearchBar;
