function Input(props) {
	const placeholder = props.placeholder;
	const labelText = props.labelText;
	const type = props.type ? props.type : 'text';

	return (
		<div className='col-auto'>
			{labelText && <label>{labelText}</label>}
			<input
				type={type}
				min='0'
				className='form-control border border-primary'
				placeholder={placeholder}
				onChange={($event) => props.onChange($event.target.value)}
			/>
		</div>
	);
}
export default Input;
