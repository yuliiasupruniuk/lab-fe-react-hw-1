function Textarea(props) {
	const placeholder = props.placeholder;
	const labelText = props.labelText;

	return (
		<div className='col-auto my-2'>
			{labelText && <label for={labelText}>{labelText}</label>}
			<textarea
				className='form-control border border-primary'
				placeholder={placeholder}
				id={labelText}
				rows='3'
				onChange={($event) => props.onChange($event.target.value)}
			></textarea>
		</div>
	);
}
export default Textarea;
