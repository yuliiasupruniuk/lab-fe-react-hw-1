function Button(props) {
	let styleClasses = [
		'btn',
		'font-weight-bold',
		'border',
		'border-primary',
		`btn-${props.color}`,
	];

	return (
		<div>
			<button
				type='button'
				className={styleClasses.join(' ')}
				onClick={() => props.onClick()}
			>
				{props.buttonText}
			</button>
		</div>
	);
}
export default Button;
