import { useState } from 'react';
import durationInHours from '../../helpers/pipeDuration';
import { ACTION_TYPES } from '../../helpers/reducer';
import Button from '../common/Button/Button';
import Input from '../common/Input/Input';
import Textarea from '../common/Textarea/Textarea';

function CreateCourse(props) {
	const [newCourse, changeNewCourse] = useState({
		title: {
			valid: false,
			touched: false,
			value: '',
		},
		description: {
			valid: false,
			touched: false,
			value: '',
		},
		duration: {
			valid: false,
			touched: false,
			value: 0,
		},
		authors: {
			valid: false,
			touched: true,
			value: [],
		},
	});

	const [newAuthor, setNewAuthor] = useState({ name: '', valid: true });
	const authors = props.authors;
	const dispatch = props.dispatch;

	const createAuthor = () => {
		const ifExist = authors.filter((author) => author.name === newAuthor.name);

		if (
			!newAuthor.name.trim() ||
			ifExist.length > 0 ||
			newAuthor.name.length < 2
		) {
			setNewAuthor((prev) => {
				return { name: prev.name, valid: false };
			});
		} else {
			dispatch({ type: ACTION_TYPES.ADD_AUTHOR, author: newAuthor.name });
		}
	};

	const setFieldValue = (field, value) => {
		let isValid = !value.trim() ? false : true;
		if (field === 'description' && value.length < 2) {
			isValid = false;
		}
		changeNewCourse((prev) => {
			return {
				...prev,
				[field]: {
					valid: isValid,
					value,
					touched: true,
				},
			};
		});
	};

	const setAuthorName = (name) => {
		setNewAuthor((prev) => {
			return { valid: true, name };
		});
	};

	const setDuration = (duration) => {
		const isValid = duration > 0 ? true : false;
		changeNewCourse((prev) => {
			return {
				...prev,
				duration: {
					valid: isValid,
					value: duration,
					touched: true,
				},
			};
		});
	};

	const updateAuthors = (authors) => {
		const isValid = authors.length > 0 ? true : false;
		changeNewCourse((prev) => {
			return {
				...prev,
				authors: {
					valid: isValid,
					value: authors,
					touched: true,
				},
			};
		});
	};

	const addAuthor = (author) => {
		const authors = [...newCourse.authors.value];
		authors.push(author);
		updateAuthors(authors);
	};

	const deleteAuthor = (authorId) => {
		const updatedAuthors = newCourse.authors.value.filter(
			(author) => author.id !== authorId
		);
		updateAuthors(updatedAuthors);
	};

	const getDurationInHours = () => {
		return durationInHours(newCourse.duration.value);
	};

	const isFormValid = () => {
		const course = Object.entries(newCourse);
		return !course.filter((field) => !field[1].touched || !field[1].valid)
			.length;
	};

	const createCourse = () => {
		if (isFormValid()) {
			const course = {
				title: newCourse.title.value,
				description: newCourse.description.value,
				duration: +newCourse.duration.value,
				authors: newCourse.authors.value.map((author) => author.id),
			};

			dispatch({ type: ACTION_TYPES.CREATE_COURSE, course });
			props.hide();
		} else {
			alert('Please, fill in all fields');
		}
	};

	const unaddedAuthors = authors.reduce((arr, author) => {
		const isAdded = newCourse.authors.value.filter(
			(courseAuthor) => author.id === courseAuthor.id
		);
		if (isAdded.length === 0) {
			return arr.concat(author);
		} else {
			return arr;
		}
	}, []);

	const authorsList = unaddedAuthors.map((author) => {
		return (
			<div className='row mb-2' key={author.id}>
				<p className='col-7'>{author.name}</p>
				<Button buttonText='Add author' onClick={() => addAuthor(author)} />
			</div>
		);
	});
	const courseAuthors = newCourse.authors.value.map((author) => (
		<div className='row mb-2' key={author.id + 1}>
			<p className='col-7'>{author.name}</p>
			<Button
				buttonText='Remove author'
				onClick={() => deleteAuthor(author.id)}
			/>
		</div>
	));

	return (
		<form className='border border-primary container-fluid p-4 mt-4'>
			<div className='d-flex justify-content-between align-items-center'>
				<div>
					<Input
						labelText='Title'
						placeholder='Enter title...'
						onChange={(val) => setFieldValue('title', val)}
					/>
					{!newCourse.title.valid && newCourse.title.touched && (
						<small className='text-danger col-auto'>Incorrect value</small>
					)}
				</div>
				<div>
					<Button buttonText='Create course' onClick={createCourse} />
				</div>
			</div>
			<Textarea
				labelText='Description'
				placeholder='Enter description'
				onChange={(val) => setFieldValue('description', val)}
			/>
			{!newCourse.description.valid && newCourse.description.touched && (
				<small className='text-danger col-auto'>
					Length should be at least 2 characters.
				</small>
			)}
			<div className='border border-warning p-3 m-4'>
				<div className='row'>
					<div className='col-6'>
						<h4 className='text-center m-3'>Add author</h4>
						<div>
							<div>
								<Input
									labelText='Author name'
									placeholder='Enter author name'
									onChange={setAuthorName}
								/>
								{!newAuthor.valid && (
									<small className='text-danger col-auto'>
										Length should be at least 2 characters.
									</small>
								)}
							</div>
							<div className='text-center mt-3'>
								<Button buttonText='Create author' onClick={createAuthor} />
							</div>
						</div>
					</div>

					<div className='col-6'>
						<h4 className='text-center m-3'>Authors</h4>
						<div>{authorsList}</div>
					</div>

					<div className='col-6 mt-3'>
						<h4 className='text-center m-3'>Duration</h4>
						<div>
							<Input
								labelText='Duration'
								placeholder='Enter duration in minutes'
								type='number'
								onChange={setDuration}
							/>
							{!newCourse.duration.valid && newCourse.duration.touched && (
								<small className='text-danger col-auto'>Incorrect value</small>
							)}
							<p>
								Duration: <strong>{getDurationInHours()}</strong> hours
							</p>
						</div>
					</div>

					<div className='col-6  mt-3'>
						<h4 className='text-center m-3'>Course authors</h4>
						<div>
							{courseAuthors.length > 0 ? (
								courseAuthors
							) : (
								<p className='text-center'>Author list is empty</p>
							)}
						</div>
					</div>
				</div>
			</div>
		</form>
	);
}
export default CreateCourse;
